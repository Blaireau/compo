# Compo

Composition algorithmique

# Piano app

## Installation

- Create a virtual environment
- Install the required packages from `requirements.txt`
- Download the soundfont from [https://musical-artifacts.com/artifacts/1390](https://musical-artifacts.com/artifacts/1390) and put it in `soundfonts/alex_gm.sf2`

## Usage

- Activate the virtual environment and launch `piano.py`
