from kivy.app import App
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.properties import (
    NumericProperty,
    ListProperty,
    ColorProperty,
    BooleanProperty,
    ObjectProperty,
)

import time

from instrument import Instrument
from audioplayer import AudioPlayer
from orchestra import Orchestra, Performer
from composers.theme import ThemeComposer
from musicsheet import Scale, Pitch


class CompoApp(App):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.player = None

    def generate_new(self):
        self.composer = ThemeComposer(
            # scale=Scale.MAJOR.with_root(Pitch.C4),
            # scale=Scale.MELODIC_MINOR.with_root(Pitch.D4),
            scale=Scale.HARMONIC_MINOR.with_root(Pitch.C4),
            # lowest_pitch=Pitch.C3,
            # highest_pitch=Pitch.C5,
        )
        self.theme = self.composer.generate()
        self.root.rythm_display.text = str(self.theme)

    def play_theme(self):
        if self.player is not None:
            self.player.stop()

        self.note_sequence = iter(self.theme)
        self.current_pitch = None

        piano = Instrument('bright piano')
        performer = Performer(piano, self.theme.to_music_sheet(tempo=90))

        # orchestra
        self.orchestra = Orchestra()
        self.orchestra.register_performer(performer, note_callback=self.play_note)

        self.player = AudioPlayer(self.orchestra)
        self.player.play()

    def on_start(self):
        self.generate_new()

    def play_note(self, time, event, sequencer, data):
        if self.current_pitch is not None:
            self.root.piano_display.press_key(self.current_pitch, False)
        try:
            self.current_pitch = next(self.note_sequence).pitch
            self.root.piano_display.press_key(self.current_pitch)
        except StopIteration:
            pass


class PianoKey(Widget):

    pressed = BooleanProperty(False)
    color = ColorProperty((1, 1, 1))
    pressed_color = ColorProperty((0, 0.5, 1))
    border_width = NumericProperty(1.5)


class PianoDisplay(RelativeLayout):

    pitch_low = ObjectProperty(Pitch.C2)
    pitch_high = ObjectProperty(Pitch.C6)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.piano_keys = {}

        self.scale = Scale.MAJOR.with_root(Pitch.C4)

        if self.pitch_low > self.pitch_high:
            raise AttributeError("A piano should have at least one note")
        if self.pitch_low not in self.scale:
            raise AttributeError("Lowest pitch should be a white key")
        if self.pitch_high not in self.scale:
            raise AttributeError("Highest pitch should be a white key")

        BLACK_KEYS_HEIGHT_RATIO = 0.6
        BLACK_KEYS_WIDTH_RATIO = 0.6
        white_pitches = list(self.scale.walk_in_range(self.pitch_low, self.pitch_high+1))
        self.white_keys_count = len(white_pitches)
        black_keys = []
        for x, pitch in enumerate(white_pitches):
            key = PianoKey(
                      pos_hint={'x': x/self.white_keys_count,
                                'y': 0},
                      size_hint=(1/self.white_keys_count, 1)
            )
            self.add_widget(key)
            self.piano_keys[pitch] = key

            next_pitch = pitch + 1
            if next_pitch in self.scale or next_pitch >= self.pitch_high:
                continue

            black_key = PianoKey(
                      color=(0, 0, 0),
                      pos_hint={'center_x': (x+1)/self.white_keys_count,
                                'y': (1-BLACK_KEYS_HEIGHT_RATIO)},
                      size_hint=(BLACK_KEYS_WIDTH_RATIO/self.white_keys_count,
                                 BLACK_KEYS_HEIGHT_RATIO)
            )
            black_keys.append(black_key)
            self.piano_keys[next_pitch] = black_key

        for black_key in black_keys:
            self.add_widget(black_key)

    def press_key(self, pitch: Pitch, pressed=True):
        key = self.piano_keys.get(pitch)
        if key is not None:
            old_state = key.pressed
            if old_state != pressed:
                key.pressed = pressed

    def on_width(self, s, width):
        self.height = 5*width/self.white_keys_count


if __name__ == '__main__':
    CompoApp().run()
