from abc import ABC, abstractmethod

from compo.musicsheet import SheetElement


class Composer(ABC):

    @abstractmethod
    def generate(self, *args, **kwargs) -> SheetElement:
        return NotImplemented


