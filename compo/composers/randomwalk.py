from compo.composer import Composer

from musicsheet import MusicSheet, StaffNote, Duration

import random

class RandomWalkComposer(Composer):

    def generate(self, piece_duration=Duration.WHOLE*8) -> MusicSheet:
        piano_part = MusicSheet(tempo=120)
        last_note = 60
        time = 0
        while time < piece_duration:
            abs_note = round(random.normalvariate(mu=60, sigma=6))
            rel_note = round(random.normalvariate(mu=last_note, sigma=8))
            alpha = 0.7
            pitch = round(alpha*rel_note + (1-alpha)*abs_note)
            last_note = pitch
            duration = random.sample([
                Duration.WHOLE,
                Duration.HALF,
                Duration.QUARTER,
                Duration.EIGTH,
            ], counts=[2, 4, 10, 4], k=1)[0]
            note = StaffNote(pitch, time, duration)
            time += duration
            if not note.is_natural and random.randint(1, 20) < 16:
                random_sign = 2*random.randint(0, 1) - 1
                note.transpose(random_sign)
            piano_part.add_note(note)
        return piano_part
