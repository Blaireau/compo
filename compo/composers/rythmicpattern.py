import random

from compo.composers.composer import Composer
from compo.musicsheet import *


class RythmicPatternComposer(Composer):

    def __init__(self,
                 beat_size: Duration=Duration.QUARTER,
                 beat_count: int=4,
                 max_complexity: int=10):
        '''
        Generate short rythmic pieces
        '''
        if beat_size <= 0:
            raise ValueError(f'beat_size should be strictly positive, got {beat_size}')
        if beat_count <= 0:
            raise ValueError(f'beat_count should be strictly positive, got {beat_count}')
        self.beat_size = beat_size
        self.beat_count = beat_count
        self.max_complexity = max_complexity

    def generate(self) -> list[MelodicPattern]:
        return list(self._make_rythmic_patterns())

    def _make_rythmic_patterns(self):
        patterns = self._generate_initial_rythmic_patterns()
        for p in patterns:
            if self._pattern_complexity(p) <= self.max_complexity:
                yield p

    def _generate_initial_rythmic_patterns(self):
        all_patterns = set()
        for smallest_duration in (Duration.EIGTH, Duration.SIXTEENTH):
            primordial = MelodicPattern(self.beat_size)
            eigth_count = self.beat_size * self.beat_count // smallest_duration
            primordial.add_notes([
                StaffNote(Pitch.SILENCE, smallest_duration)
                for _ in range(eigth_count)
            ])
            patterns = [primordial]
            half_generations = eigth_count // 2
            # expand from primordial, doubling the number of patterns each generation
            for _ in range(half_generations):
                all_patterns.update(patterns)
                patterns = [self._mutate_rythmic_pattern(p) for p in 2*patterns]
            # continue expansion slowly, taking only half of the previous generation
            for _ in range(half_generations):
                all_patterns.update(patterns)
                patterns = [self._mutate_rythmic_pattern(p) for p in patterns[::2]]
        return all_patterns

    def _mutate_rythmic_pattern(self, pattern: MelodicPattern) -> MelodicPattern:
        if len(pattern) <= 1:
            return pattern
        index = random.randrange(len(pattern)-1)
        merged_duration = pattern[index].duration + pattern[index+1].duration
        new_pattern = MelodicPattern(pattern.pulsation)
        new_pattern.add_notes(pattern[:index])
        new_pattern.add_note(StaffNote(Pitch.SILENCE, merged_duration))
        new_pattern.add_notes(pattern[index+2:])
        return new_pattern

    def _pattern_complexity(self, pattern: MelodicPattern) -> int:
        '''
        Compute the rythmic complexity of a pattern

        A complexity equal to 0 is the most simple pattern (holding the same
        note for the entire pattern) Higher values indicate more diversity and
        difficulty to reproduce the pattern.
        A good estimate is of the complexity score is:
            0 - 10: simple rythme
            10 - 20: quite complex rythme
        Anything above 20 is probably too much.

        The following heuristic criterias are used to estimate complexity:
            - off-beat notes add more complexity than on-beat notes, especially
              if the previous beat is not marked
            - sequences of notes of different durations add more complexity
              than sequences of notes of the same duration
            - non-canonical durations (notes that need a tie to be represented
              on a music sheet) add more complexity than canonical durations
        '''
        score = 0
        previous_note, *remain = pattern
        last_beat_marked = 0
        for note in remain:
            on_beat = note.start_time % pattern.pulsation == 0
            current_beat = note.start_time // pattern.pulsation
            score += self._note_complexity_score(
                on_beat=on_beat,
                same_duration=previous_note.duration == note.duration,
                dist_prev_beat_marked=current_beat - last_beat_marked,
                is_canonical=Duration.is_canonical(note.duration)
            )
            previous_note = note
            if on_beat:
                last_beat_marked = current_beat
        return score

    def _note_complexity_score(self, on_beat: bool,
                                    same_duration: bool,
                                    dist_prev_beat_marked: int,
                                    is_canonical: bool) -> int:
        # /!\ CHANGE WITH CAUTION
        base_score = 0 if is_canonical else 3
        if on_beat:
            sequence_score = 0 if same_duration else 2
        else:
            if dist_prev_beat_marked == 0:
                sequence_score = 1 if same_duration else 4
            else:
                sequence_score = 6*dist_prev_beat_marked
        return base_score + sequence_score

