import random

from compo.composers.composer import Composer
from compo.composers.motive import MotiveComposer
from compo.musicsheet import *


'''
Rules of Melody
The purpose of the rules of melody as described in this text are to aid in the construction of singable melodies and to reflect a summary of melodic practice in music.

# 1) Tendency tones (scale degrees 2, 4, 6, 7) should resolve:
#  7 goes to 8
#  and, to a lesser extent, 4 goes to 3
#  (scale degrees 6 and 2 both should resolve down by step)

2) Avoid augmented intervals
9) Avoid highlighting tritones with step sequences

3) Leaps larger than a 4th and leaps of diminished intervals should change direction after the leap
6) Prepare AND/OR resolve leaps

4) Consecutive leaps should outline a triad

5) Punctuation = Cadence /!\ Important !


7) One unique apogee, do not reach or leave it by leap

8) One unique perigee, can be reached / left by leap


10) No tone should appear more than 3 times

Period form:
Basic Idea, harmonicaly stable, stay on I (2bars) | Contrasting Idea, question (2bars) |
Basic Idea, repeated | Cadence, simple (2bars)

Sentence form:
Basic Idea(2bars) | Repeat Idea(2bars) |
Continuation(1 bar idea x2) | Cadence, simplify and go home (2bars)

Basic idea: always start on I
Cadence: finish with 2 -> 1 OR 7 -> 8


Control patterns:
    - rythmic complexity
    - sentence form
    - tonality
    - range (absolute height [min, max])
    - power (leap more often)
'''

INFINITE_COST = 100000

class ThemeComposer(Composer):

    def __init__(self,
                 scale: Scale.MAJOR,
                 lowest_pitch: Pitch=Pitch.C0,
                 highest_pitch: Pitch=Pitch.C6,
                 jump_power: int=1,
                 max_energy: int=8,
                 beat_size: Duration=Duration.QUARTER,
                 beat_count: int=4,
        ):
        self.scale = scale
        all_pitches = list(self.scale.walk_in_range(lowest_pitch, highest_pitch))
        self.pitch_pool = {
            pitch: 5 if abs(pitch - self.scale.root) < Interval.FIFTH.semitones else 3
            for pitch in all_pitches
        }

        motive_composer = MotiveComposer(
        )
        motives = motive_composer.generate()
        print([m.duration // beat_size for m in motives])

        basic_idea = MelodicPattern()
        a = motives[0].materialize_from(self.scale.root, self.scale)
        b = motives[0].materialize_from(a[-1].pitch, self.scale)
        c = motives[0].materialize_from(self.scale.root, self.scale)
        d = motives[0].materialize_from(c[-1].pitch, self.scale)
        (
        basic_idea.add_notes(a)
                  .add_notes(b)
                  .add_notes(c)
                  .add_notes(d)
        )
        # self.fill_note(basic_idea[0], None, constraint_step={1, 3, 5})
        # self.fill_notes(basic_idea[1:], basic_idea[0])
        # contrast = MelodicPattern()
        # s, t = random.sample((random.choice((a, b)), c), k=2)
        # (
        # contrast.add_notes(s)
        #         .add_notes(t)
        #         .add_notes(t)
        #         .add_notes(s)
        # )
        # self.fill_notes(contrast, basic_idea[-1])
        # u = random.choice((a, b))
        # e = self.find_good_end_rythm(rythms)
        # cadence = MelodicPattern()
        # (
        # cadence.add_notes(u)
        #        .add_notes(e)
        #        .add_notes(u)
        #        .add_notes(e)
        # )
        # self.fill_notes(cadence[:-2], basic_idea[-1])
        # self.fill_note(cadence[-2], cadence[-3], constraint_step={2, 7})
        # self.fill_note(cadence[-1], cadence[-2], constraint_step={1, 8})
        self.theme = MelodicPattern()
        # TODO: continuity / note count between parts
        (
        self.theme.add_notes(basic_idea)
                  # .add_notes(contrast)
                  # .add_notes(basic_idea)
                  # .add_notes(cadence)
        )

    def find_good_end_rythm(self, rythms):
        return random.choice([
            r for r in rythms if r[-1].duration >= Duration.QUARTER and len(r) <= 5
        ])

    def fill_note(self,
                  note: StaffNote,
                  previous_note: StaffNote | None,
                  constraint_step: set[int]=None):
        pitch = Pitch.C4
        # if previous_note is not None:
        #     pitch += Pitch.E4 - previous_note.pitch
        note.pitch = pitch
        return
        # filter valid pitches
        pitches = []
        counts = []
        for pitch, count in self.pitch_pool.items():
            if count == 0:
                continue
            if self.energy < self.jump_energy_cost(note, pitch, previous_note):
                continue
            if constraint_step is not None:
                if self.scale.get_index_in_scale(pitch) not in constraint_step:
                    continue
            pitches.append(pitch)
            counts.append(count)
        # select a pitch and apply it
        pitch = random.sample(pitches, counts=counts, k=1)[0]
        note.pitch = pitch
        if previous_note is None or previous_note.pitch != pitch:
            self.pitch_pool[pitch] -= 1
        # update energy
        cost = self.jump_energy_cost(note, pitch, previous_note)
        regen = self.jump_power * note.duration / self.beat_size
        self.energy = min(self.max_energy, self.energy - cost + regen)

    def jump_energy_cost(self,
                         target_note: StaffNote,
                         target_pitch: Pitch,
                         previous_note: StaffNote | None):
        if previous_note is None:
            return 0
        dist = target_pitch - previous_note.pitch
        will_go_up = dist > 0
        if abs(dist) > Interval.OCTAVE.semitones:
            return INFINITE_COST
        interval = Interval.from_semitones(abs(dist))
        if interval.quality is Quality.TRITON:
            return INFINITE_COST
        cost = 0
        change_dir_is_costly = True
        keep_dir_is_costly = False
        if interval.is_leap:
            cost += interval.semitones
            change_dir_is_costly = False
            if interval.semitones >= Interval.FIFTH.semitones:
                keep_dir_is_costly = True
        if change_dir_is_costly and (will_go_up != self.is_going_up):
            cost += Duration.QUARTER / target_note.duration
        if keep_dir_is_costly and (will_go_up == self.is_going_up):
            cost += Duration.HALF / target_note.duration
        return cost

    def fill_notes(self,
                   pattern: MelodicPattern,
                   previous_note: StaffNote=None):
        for note in pattern:
            self.fill_note(note, previous_note)
            previous_note = note


    def generate(self) -> MelodicPattern:
        return self.theme
