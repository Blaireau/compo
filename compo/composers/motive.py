import random
import math
from itertools import zip_longest, chain

from compo.composers.composer import Composer
from compo.musicsheet import *


class MotiveGenerationError(Exception):
    pass


class Motive:

    def __init__(self, rythm, intervals):
        if len(rythm) != len(intervals) + 1:
            raise ValueError(f'A motive of n notes should have n-1 intervals')
        self.rythm = rythm
        self.intervals = intervals

    @property
    def duration(self):
        return sum(self.rythm)

    def materialize_from(self, starting_pitch: Pitch, scale: Scale) -> MelodicPattern:
        melody = MelodicPattern()
        pitch = starting_pitch
        for duration, interval in zip_longest(self.rythm, self.intervals, fillvalue=0):
            melody.add_note(StaffNote(pitch, duration))
            pitch = scale.nearest_pitch(pitch + interval)
        return melody


class MotiveComposer(Composer):
    STANDARD_RYTHMS = [
        [Duration.WHOLE],
        [Duration.HALF_DOT],
        [Duration.HALF],
        [Duration.QUARTER],
        [Duration.EIGTH for _ in range(2)],
        [Duration.SIXTEENTH for _ in range(4)],
    ]

    SPICY_RYTHMS = [
        [Duration.EIGTH_DOT, Duration.SIXTEENTH],
        [Duration.QUARTER_DOT, Duration.EIGTH],
        [Duration.SIXTEENTH, Duration.EIGTH_DOT],
        [Duration.EIGTH, Duration.QUARTER_DOT],
        [Duration.EIGTH, Duration.SIXTEENTH, Duration.SIXTEENTH],
        [Duration.SIXTEENTH, Duration.SIXTEENTH, Duration.EIGTH],
        [Duration.SIXTEENTH, Duration.EIGTH, Duration.SIXTEENTH],
        [Duration.EIGTH, Duration.QUARTER, Duration.EIGTH],
    ]

    # if a rythmic block deviates from this much from the target notes_per_beat,
    # it’s chances of being picked are divided by 2
    HALF_CHANCE_DEVIATION = 0.5
    # if a rythmic block deviates from this much from the target notes_per_beat,
    # it’s considered invalid
    MAX_DEVIATION = 0.8

    MAX_RETRIES = 200

    STEP_INTERVALS = [
        Interval.UNISON,
        Interval.MINOR_SECOND,
        Interval.MAJOR_SECOND,
        Interval.MINOR_THIRD,
        Interval.MAJOR_THIRD,
    ]

    LEAP_INTERVALS = [
        Interval.FOURTH,
        Interval.FIFTH,
        Interval.MINOR_SIXTH,
        Interval.MAJOR_SIXTH,
        Interval.MINOR_SEVENTH,
        Interval.MAJOR_SEVENTH,
        Interval.OCTAVE,
    ]

    UNISON_WEIGHT = 2
    OCTAVE_WEIGHT = 3

    MIN_NOTES = 4
    MAX_NOTES = 7

    def __init__(self,
                 motives_number=3,
                 beat_size: Duration=Duration.QUARTER,
                 spicy_rythms=1,
                 notes_per_beat=1.8,
                 leaps=1,
                 direction_change_prob=0.8,
                 intervals_stats=Scale.MAJOR.intervals_stats
        ):
        self.spicy_rythms = spicy_rythms
        self.notes_per_beat = notes_per_beat
        self.leaps = leaps
        self.motives_number = motives_number
        self.beat_size = beat_size
        self.direction_change_prob = direction_change_prob
        self._standard_dist = self.compute_distribution(self.STANDARD_RYTHMS)
        self._spicy_dist = self.compute_distribution(self.SPICY_RYTHMS)
        self._steps = []
        self._leaps = []
        self._steps_dist = []
        self._leaps_dist = []
        for interval, count in intervals_stats.items():
            if interval.quality is Quality.TRITON:
                continue
            if interval is Interval.UNISON:
                count = self.UNISON_WEIGHT
            if interval is Interval.OCTAVE:
                count = self.OCTAVE_WEIGHT
            if interval.is_step:
                self._steps.append(interval)
                self._steps_dist.append(count)
            else:
                self._leaps.append(interval)
                self._leaps_dist.append(count)

    def generate(self) -> list[Motive]:
        rythmic_motives = self.with_max_retries(self.generate_rythmic_motives)
        motive_lengths = [len(m)-1 for m in rythmic_motives]
        interval_motives = self.with_max_retries(
            lambda: self.generate_interval_motives(motive_lengths)
        )
        motives = [
            Motive(rythm, intervals)
            for rythm, intervals in zip(rythmic_motives, interval_motives)
        ]
        return motives

    def with_max_retries(self, method):
        last_error = None
        for x in range(self.MAX_RETRIES):
            try:
                return method()
            except MotiveGenerationError as er:
                last_error = er
        raise last_error

    # INTERVAL MOTIVE ----------------------------------------------------------

    def get_random_step_interval(self):
        return random.sample(self._steps, counts=self._steps_dist, k=1)[0]

    def get_random_leap_interval(self):
        return random.sample(self._leaps, counts=self._leaps_dist, k=1)[0]

    def is_valid_sequence(self, sequence: list[int]) -> bool:
        dir_int = [
            (int(math.copysign(1, interval)), interval)
            for interval in sequence
            if interval != 0
        ]
        if len(dir_int) == 0:
            return True
        directions, intervals = zip(*dir_int)
        iter_dirs = zip_longest(
            [None, *directions[:-1]],
            directions,
            intervals,
            directions[1:],
        )
        for prev_dir, curr_dir, interval, next_dir in iter_dirs:
            if (abs(interval) >= Interval.FIFTH.semitones
                and prev_dir == curr_dir == next_dir):
                return False
        return True

    def generate_interval_motives(self, lengths: list[int]):
        motives = [self.generate_interval_base_sequence(l) for l in lengths]
        for _ in range(self.leaps):
            motive = random.choice(motives)
            index = random.randrange(len(motive))
            leap_amplitude = self.get_random_leap_interval().semitones
            leap = int(math.copysign(leap_amplitude, -motive[index]))
            motive[index] = leap
        for motive in motives:
            if not self.is_valid_sequence(motive):
                raise MotiveGenerationError(
                    'Failed to generate interval sequence'
                )
        return motives

    def generate_interval_base_sequence(self, length: int):
        sequence = []
        direction = random.choice((-1, 1))
        for _ in range(length):
            interval = direction*self.get_random_step_interval().semitones
            sequence.append(interval)
            if random.random() < self.direction_change_prob:
                direction *= -1
        return sequence


    # RYTHMIC MOTIVE ----------------------------------------------------------

    def generate_rythmic_motives(self):
        rythmic_motives = [[] for _ in range(self.motives_number)]
        for _ in range(self.spicy_rythms):
            spice = self.get_random_spicy_rythm()
            random.choice(rythmic_motives).append(spice)
        complete_rythmic_motives = [
            self.complete_rythmic_motive(motive)
            for motive in rythmic_motives
        ]
        for motive in complete_rythmic_motives:
            random.shuffle(motive)
        final_motives = [
            list(chain.from_iterable(motive))
            for motive in complete_rythmic_motives
        ]
        return final_motives

    def compute_distribution(self, rythmic_blocks):
        AMPLITUDE = 100
        LAMBDA = self.HALF_CHANCE_DEVIATION / math.log(2)
        distr = []
        for block in rythmic_blocks:
            notes, block_notes_per_beat = self.rythmic_sequence_properties(block)
            delta = abs(block_notes_per_beat - self.notes_per_beat)
            score = round(AMPLITUDE*math.exp(-delta/LAMBDA))
            distr.append(score)
        return distr

    def get_random_standard_rythm(self):
        return random.sample(self.STANDARD_RYTHMS, counts=self._standard_dist, k=1)[0]

    def get_random_spicy_rythm(self):
        return random.sample(self.SPICY_RYTHMS, counts=self._spicy_dist, k=1)[0]

    def rythmic_motive_properties(self, motive: list[list[Duration]]):
        if motive is None:
            return None, None
        return self.rythmic_sequence_properties(list(chain.from_iterable(motive)))

    def rythmic_sequence_properties(self, seq: list[Duration]):
        notes = len(seq)
        beats = sum(seq) // self.beat_size
        if beats == 0:
            return notes, 0
        notes_per_beat = notes / beats
        return notes, notes_per_beat

    def complete_rythmic_motive(self, motive: list[list[Duration]]):
        note_count, _ = self.rythmic_motive_properties(motive)
        best_motive = None
        best_delta = float('inf')
        while note_count < self.MAX_NOTES:
            added = self.get_random_standard_rythm()
            motive.append(added)
            note_count, notes_per_beat = self.rythmic_motive_properties(motive)
            delta = abs(notes_per_beat - self.notes_per_beat)
            if note_count <= self.MAX_NOTES and delta < best_delta:
                best_delta = delta
                best_motive = list(motive)
        note_count, notes_per_beat = self.rythmic_motive_properties(best_motive)
        if (best_motive is None
            or not (self.MIN_NOTES <= note_count <= self.MAX_NOTES)
            or abs(notes_per_beat - self.notes_per_beat) > self.MAX_DEVIATION):
            raise MotiveGenerationError('Failed to generate rythm')
        return best_motive



