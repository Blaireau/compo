import pyaudio

from orchestra import Orchestra


class AudioPlayer:

    def __init__(self, orchestra: Orchestra):
        self.orchestra = orchestra
        self.pa = pyaudio.PyAudio()
        self.stream = self.pa.open(
            format = pyaudio.paInt16,
            frames_per_buffer=1024,
            channels = 2,
            rate = 44100,
            output = True,
            start = False,
            stream_callback=self.audio_stream
        )

    def audio_stream(self, in_data, frame_count, time_info, status):
        samples = self.orchestra.get_samples(frame_count)
        return (samples, pyaudio.paContinue)

    def play(self):
        self.stream.start_stream()

    def pause(self):
        self.stream.stop_stream()

    def stop(self):
        self.stream.close()
        self.pa.terminate()
