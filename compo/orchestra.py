import fluidsynth

from dataclasses import dataclass

from musicsheet import MusicSheet
from instrument import Instrument

SOUNDFONT_FILE = "soundfonts/alex_gm.sf2"



@dataclass
class Performer:
    instrument: Instrument
    part: MusicSheet


class Orchestra:

    def __init__(self):
        self.fs = fluidsynth.Synth()
        self.alex = self.fs.sfload(SOUNDFONT_FILE)
        self.sequencer = fluidsynth.Sequencer(use_system_timer=False)
        self.synthID = self.sequencer.register_fluidsynth(self.fs)
        self._performers = []
        self.piece_end_time = 0

    def get_samples(self, n: int):
        return fluidsynth.raw_audio_string(self.fs.get_samples(n))

    def register_performer(self, performer, note_callback=None):
        channel = len(self._performers)
        self.fs.program_select(channel, self.alex, 0, performer.instrument.id)
        self._performers.append(performer)
        if note_callback is not None:
            callback_id = self.sequencer.register_client("noteCallback", note_callback)
        for note in performer.part.iter_midi():
            self.sequencer.note(time=note.start_time,
                                channel=channel,
                                key=note.pitch,
                                velocity=note.velocity,
                                duration=note.duration,
                                dest=self.synthID)
            self.piece_end_time = max(self.piece_end_time, note.end_time)
            if note_callback is not None:
                self.sequencer.timer(note.start_time, dest=callback_id)
        if note_callback is not None:
            self.sequencer.timer(note.end_time, dest=callback_id)

    def stop(self):
        self.fs.delete()
