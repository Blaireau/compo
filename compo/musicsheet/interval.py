from enum import Enum, unique

from .midi import Pitch


@unique
class Quality(Enum):
    PERFECT = 1
    MAJOR = 2
    MINOR = 3
    TRITON = 4


@unique
class Interval(Enum):
    UNISON        = ( 0, Quality.PERFECT)
    MINOR_SECOND  = ( 1, Quality.MINOR)
    MAJOR_SECOND  = ( 2, Quality.MAJOR)
    MINOR_THIRD   = ( 3, Quality.MINOR)
    MAJOR_THIRD   = ( 4, Quality.MAJOR)
    FOURTH        = ( 5, Quality.PERFECT)
    TRITON        = ( 6, Quality.TRITON)
    FIFTH         = ( 7, Quality.PERFECT)
    MINOR_SIXTH   = ( 8, Quality.MINOR)
    MAJOR_SIXTH   = ( 9, Quality.MAJOR)
    MINOR_SEVENTH = (10, Quality.MINOR)
    MAJOR_SEVENTH = (11, Quality.MAJOR)
    OCTAVE        = (12, Quality.PERFECT)

    def __init__(self, semitones: int, quality: Quality):
        self.semitones = semitones
        self.quality = quality

    @property
    def shortname(self):
        if '_' in self.name:
            code, name = self.name.split('_')
            suffix = 'M' if self.quality is Quality.MAJOR else 'm'
            return f'{name}_{suffix}'
        else:
            return f'{self.name}'

    @property
    def is_step(self):
        return self.semitones < Interval.FOURTH.semitones

    @property
    def is_leap(self):
        return not self.is_step

    @classmethod
    def from_semitones(cls, semitones: int):
        abs_semitones = abs(semitones)
        for interval in cls:
            if interval.semitones == abs_semitones:
                return interval
        raise ValueError(f'No known interval with {semitones} semitones')

    def __str__(self):
        return f'{self.name}'
