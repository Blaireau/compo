from dataclasses import dataclass
from enum import IntEnum

MIDI_LOWEST_PITCH = 12
MIDI_OCTAVE_COUNT = 10
BASE_NAMES_OFFSETS = {
    'C': 0,
    'D': 2,
    'E': 4,
    'F': 5,
    'G': 7,
    'A': 9,
    'B': 11,
}

def _build_pitches_dict():
    for octave in range(MIDI_OCTAVE_COUNT):
        for note_name, offset in BASE_NAMES_OFFSETS.items():
            height = MIDI_LOWEST_PITCH + offset + 12*octave
            yield f'{note_name}{octave}', height
    yield 'SILENCE', -1

Pitch = IntEnum('Pitch', dict(_build_pitches_dict()))


@dataclass(slots=True)
class MidiNote:
    pitch: Pitch
    velocity: int
    start_time: int
    duration: int

    def __post_init__(self):
        assert isinstance(self.pitch, int)
        assert isinstance(self.velocity, int)
        assert isinstance(self.start_time, int)
        assert isinstance(self.duration, int)

    @property
    def end_time(self):
        return self.start_time + self.duration
