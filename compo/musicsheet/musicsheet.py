from abc import ABC, abstractmethod
from dataclasses import dataclass

from .duration import Duration
from .midi import Pitch, MidiNote


class SheetElement(ABC):
    pass


@dataclass(slots=True)
class StaffNote(SheetElement):
    pitch: Pitch
    duration: Duration
    start_time: int | None = None

    @property
    def end_time(self):
        if self.start_time is None:
            return None
        return self.start_time + self.duration

    @property
    def is_natural(self) -> bool:
        """
        whether this note is written without a sharp/flat

        We assume the tonality is C Major.
        """
        index_in_scale = self.pitch % 12
        return index_in_scale in (0, 2, 4, 5, 7, 9, 11)

    def __hash__(self):
        return hash((self.pitch, self.duration))

    def transpose(self, semitones: int):
        if self.pitch == Pitch.SILENCE:
            return
        self.pitch += semitones

    def to_midi(self, tempo: int, pulsation: Duration) -> MidiNote:
        scale = 1000*60/(tempo*pulsation)
        return MidiNote(
            pitch=self.pitch,
            velocity=100,
            start_time=round(self.start_time*scale),
            duration=round(self.duration*scale),
        )


class NoteCollection(ABC):

    @abstractmethod
    def add_note(self, note: StaffNote):
        return NotImplemented


class MelodicPattern(NoteCollection, SheetElement):

    def __init__(self, pulsation: Duration=Duration.QUARTER):
        self.pulsation = pulsation
        self._notes = []
        self.time = 0

    def add_note(self, note: StaffNote):
        if not isinstance(note, StaffNote):
            raise ValueError(f'note should be a StaffNote')
        self._notes.append(
            StaffNote(note.pitch, note.duration, start_time=self.time)
        )
        self.time += note.duration
        return self

    def add_notes(self, notes: list[StaffNote] | NoteCollection):
        for note in notes:
            self.add_note(note)
        return self

    def __iter__(self):
        return iter(self._notes)

    def __len__(self):
        return len(self._notes)

    def __getitem__(self, index):
        return self._notes[index]

    def __eq__(self, other):
        if not isinstance(other, MelodicPattern):
            return False
        return self._notes == other._notes

    def __hash__(self):
        return hash(tuple(self._notes))

    def __repr__(self):
        rythm = ''.join([
            'o'+'-'*(note.duration//Duration.SIXTEENTH-1)
            for note in self._notes
        ])
        beats = '┃   │   │   │   '*(self.time//(self.pulsation*4))
        return f'{rythm}\n{beats}'

    def to_music_sheet(self, tempo: int=60):
        sheet = MusicSheet(tempo, self.pulsation)
        sheet.add_notes(self._notes)
        return sheet


class MusicSheet(SheetElement):

    def __init__(self, tempo: int=60, pulsation: Duration=Duration.QUARTER):
        self.notes = []
        self.tempo = tempo
        self.pulsation = pulsation

    def add_notes(self, notes):
        self.notes.extend(notes)
        return self

    def iter_midi(self):
        return (n.to_midi(tempo=self.tempo, pulsation=self.pulsation)
                for n in self.notes)
