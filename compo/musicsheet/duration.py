from enum import IntEnum


class Duration(IntEnum):
    WHOLE_DOT = 96
    WHOLE = 64
    HALF_DOT = 48
    HALF = 32
    QUARTER_DOT = 24
    QUARTER = 16
    EIGTH_DOT = 12
    EIGTH = 8
    SIXTEENTH = 4

    @classmethod
    def is_canonical(cls, duration: int):
        return duration in [
            cls.WHOLE_DOT,
            cls.WHOLE,
            cls.HALF_DOT,
            cls.HALF,
            cls.QUARTER_DOT,
            cls.QUARTER,
            cls.EIGTH_DOT,
            cls.EIGTH,
            cls.SIXTEENTH,
        ]
