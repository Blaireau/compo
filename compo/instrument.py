import json

from dataclasses import dataclass

with open("soundfonts/alex_gm_instruments.json", "r") as file:
    INSTRUMENT_IDS = json.load(file)

@dataclass
class Instrument:
    name: str

    def __post_init__(self):
        try:
            self.id = INSTRUMENT_IDS[self.name]
        except KeyError:
            raise ValueError(f'Invalid instrument name {self.name}')
