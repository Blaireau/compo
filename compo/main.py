from instrument import Instrument
from audioplayer import AudioPlayer
from orchestra import Orchestra, Performer
from composers.theme import ThemeComposer
from musicsheet import Scale, Pitch, Interval

from tabulate import tabulate

def compute_counts(scale):
    return [count for name, count in sorted(scale.intervals_stats.items(),
                                            key=lambda t: t[0].semitones)
    ]

if __name__ == '__main__':
    data = [
        [scale.name, *compute_counts(scale)]
        for scale in Scale
    ]

    print(tabulate(data, headers=[interval.shortname for interval in Interval],
                   tablefmt='simple_outline',
                   )
    )
