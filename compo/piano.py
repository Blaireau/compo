from kivy.app import App
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.properties import NumericProperty, ListProperty, ColorProperty, BooleanProperty

import fluidsynth

from instrument import Instrument

NOTE_DISPLAY_OFFSET = 8 # must be even
MIDDLE_C_OCTAVE_OFFSET = -1
PLAYING_WHITE_NOTES = 12
DEFAULT_INSTRUMENT = 'bright piano'

DEFAULT_KEYMAP = {
    # home row
    97: 2,
    117: 4,
    105: 6,
    101: 8,
    44: 10,
    99: 12,
    116: 14,
    115: 16,
    114: 18,
    110: 20,
    109: 22,
    231: 24,
    # top row
    98: 1,
    233: 3,
    112: 5,
    111: 7,
    232: 9,
    1073741824: 11,
    118: 13,
    100: 15,
    108: 17,
    106: 19,
    122: 21,
    119: 23,
}

# control keys
TRANSLATE_DOWN = 234
TRANSLATE_UP = 102
OCTAVE_TRANSPOSE_LOW = 46
OCTAVE_TRANSPOSE_NORMAL = 107
OCTAVE_TRANSPOSE_HIGH = 39

SOUNDFONT = "soundfonts/alex_gm.sf2"


def is_natural_semitone(key_index):
    return key_index % 14 in (5, 13)

def key_index_to_note(key_index):
    if is_natural_semitone(key_index):
        raise ValueError('Invalid note')
    octave = key_index // 14
    offset = key_index % 14
    if offset < 5:
        return 12*octave + offset
    else:
        return 12*octave + offset - 1


class MidiPlayer:

    def __init__(self):
        self.fs = fluidsynth.Synth()
        self.fs.start()
        self.alex = self.fs.sfload(SOUNDFONT)
        self.set_instrument(Instrument(DEFAULT_INSTRUMENT))

    def set_instrument(self, instrument: Instrument):
        self.fs.program_select(1, self.alex, 0, instrument.id)

    def play_note(self, note, play=True):
        VELOCITY = 100
        if play:
            self.fs.noteon(1, note, VELOCITY)
        else:
            self.fs.noteoff(1, note)

    def stop(self):
        self.fs.delete()


class PianoApp(App):
    last_note = NumericProperty(0)

    def change_instrument(self, instrument_name):
        instrument = Instrument(instrument_name)
        self.root.piano_display.player.set_instrument(instrument)

    def on_start(self):
        self.root.spinner.text = DEFAULT_INSTRUMENT


class PianoKey(Widget):

    pressed = BooleanProperty(False)
    color = ColorProperty((1, 1, 1))
    pressed_color = ColorProperty((0, 0.5, 1))
    border_width = NumericProperty(1.5)


class PianoDisplay(RelativeLayout):

    notes = NumericProperty(24)
    playing_offset = NumericProperty(6)
    octave_transpose = NumericProperty(-1)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def translate_offset(self, increment):
        self.playing_offset = min(max(self.playing_offset + increment, 0), self.notes - PLAYING_WHITE_NOTES)

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        raw_key_index = self.keymap.get(keycode[0])
        if raw_key_index is not None:
            self.press_key(self.raw_to_key_index(raw_key_index))
        if keycode[0] == TRANSLATE_UP:
            self.release_all_notes()
            self.translate_offset(1)
        elif keycode[0] == TRANSLATE_DOWN:
            self.release_all_notes()
            self.translate_offset(-1)
        elif keycode[0] == OCTAVE_TRANSPOSE_LOW:
            self.release_all_notes()
            self.octave_transpose = -1
        elif keycode[0] == OCTAVE_TRANSPOSE_NORMAL:
            self.release_all_notes()
            self.octave_transpose = 0
        elif keycode[0] == OCTAVE_TRANSPOSE_HIGH:
            self.release_all_notes()
            self.octave_transpose = 1

        # Keycode is composed of an integer + a string
        # If we hit escape, release the keyboard
        if keycode[1] == 'escape':
            quit()

        # Return True to accept the key. Otherwise, it will be used by
        # the system.
        return True

    def _on_keyboard_up(self, keyboard, keycode):
        raw_key_index = self.keymap.get(keycode[0])
        if raw_key_index is not None:
            self.press_key(self.raw_to_key_index(raw_key_index), pressed=False)

        # Return True to accept the key. Otherwise, it will be used by
        # the system.
        return True

    def raw_to_key_index(self, raw_key_index: int):
        return raw_key_index + NOTE_DISPLAY_OFFSET + self.playing_offset*2

    def press_key(self, key_index: int, pressed=True):
        key = self.piano_keys.get(key_index)
        if key is not None:
            old_state = key.pressed
            if old_state != pressed:
                key.pressed = pressed
                note = 36 + 12*self.octave_transpose + key_index_to_note(key_index)
                self.player.play_note(note, play=pressed)
                if pressed:
                    self.app.last_note = note

    def release_all_notes(self):
        for key_index, key in self.piano_keys.items():
            self.press_key(key_index, pressed=False)

    def __init__(self, notes=None, **kwargs):
        super().__init__(**kwargs)
        # sound
        self.player = MidiPlayer()
        # keyboard
        self._keyboard = Window.request_keyboard(
            self._keyboard_closed, self, 'text')
        self._keyboard.bind(on_key_down=self._on_keyboard_down,
                            on_key_up=self._on_keyboard_up)

        self.keymap = DEFAULT_KEYMAP
        self.piano_keys = {}

        if notes is not None:
            if notes < 1:
                raise AttributeError("A piano should have at least one note")
            self.notes = notes

        BLACK_KEYS_HEIGHT_RATIO = 0.6
        BLACK_KEYS_WIDTH_RATIO = 0.6
        for x in range(self.notes):
            key_index = 2*(x+1) + NOTE_DISPLAY_OFFSET
            key = PianoKey(
                      pos_hint={'x': x/self.notes,
                                'y': 0},
                      size_hint=(1/self.notes, 1)
            )
            self.add_widget(key)
            self.piano_keys[key_index] = key
        for x in range(1, self.notes):
            key_index = 2*x+1 + NOTE_DISPLAY_OFFSET
            if is_natural_semitone(key_index):
                continue
            key = PianoKey(
                      color=(0, 0, 0),
                      pos_hint={'center_x': x/self.notes,
                                'y': (1-BLACK_KEYS_HEIGHT_RATIO)},
                      size_hint=(BLACK_KEYS_WIDTH_RATIO/self.notes,
                                 BLACK_KEYS_HEIGHT_RATIO)
            )
            self.add_widget(key)
            self.piano_keys[key_index] = key

    def on_width(self, s, width):
        self.height = 5*width/self.notes


if __name__ == '__main__':
    PianoApp().run()
