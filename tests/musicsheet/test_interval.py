from compo.musicsheet import Pitch, Scale

def test_reference_c4():
    assert Pitch.C4 == 60

def test_c_major_scale_contains():
    c_major = Scale.MAJOR.with_root(Pitch.C0)
    expected_pitches = [
        Pitch.C4,
        Pitch.D4,
        Pitch.E4,
        Pitch.F4,
        Pitch.G4,
        Pitch.A4,
        Pitch.B4,
        Pitch.C5,
    ]

    for pitch in expected_pitches:
        assert pitch in c_major

    excluded_pitches = [
        Pitch.C4+1,
        Pitch.D4+1,
        Pitch.F4+1,
        Pitch.G4+1,
        Pitch.A4+1,
    ]

    for pitch in excluded_pitches:
        assert pitch not in c_major

def test_c_major_scale_walk():
    c_major = Scale.MAJOR.with_root(Pitch.C5)
    expected_pitches = [
        Pitch.C4,
        Pitch.D4,
        Pitch.E4,
        Pitch.F4,
        Pitch.G4,
        Pitch.A4,
        Pitch.B4,
        Pitch.C5,
    ]

    result = list(c_major.walk_in_range(Pitch.C4, Pitch.C5+1))
    assert len(result) == len(expected_pitches)
    for res, expected in zip(expected_pitches, expected_pitches):
        assert res == expected

def test_c_major_scale_get_index():
    c_major = Scale.MAJOR.with_root(Pitch.C5)
    assert c_major.get_index_in_scale(Pitch.C4) == 1
    assert c_major.get_index_in_scale(Pitch.E4) == 3
    assert c_major.get_index_in_scale(Pitch.F5) == 4
    assert c_major.get_index_in_scale(Pitch.C6) == 1
