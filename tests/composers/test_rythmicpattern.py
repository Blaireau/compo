import pytest

from compo.musicsheet import (
    Pitch,
    Duration,
    StaffNote,
    MelodicPattern,
)
from compo.composers.rythmicpattern import RythmicPatternComposer


@pytest.fixture
def comp():
    return RythmicPatternComposer()

def make_pattern(durations: list[Duration]) -> MelodicPattern:
    pattern = MelodicPattern(pulsation=Duration.QUARTER)
    pattern.add_notes([StaffNote(Pitch.SILENCE, d) for d in durations])
    return pattern

def test_pattern_complexity(comp):
    p1 = make_pattern([
        Duration.WHOLE,
    ])
    assert comp._pattern_complexity(p1) == 0

    p2 = make_pattern([
        Duration.EIGTH,
        Duration.HALF,
        Duration.EIGTH,
        Duration.QUARTER,
    ])
    assert comp._pattern_complexity(p2) == 18

    p3 = make_pattern([
        Duration.QUARTER,
        Duration.QUARTER,
        Duration.HALF,
    ])
    assert comp._pattern_complexity(p3) == 2

    p4 = make_pattern([
        Duration.QUARTER_DOT,
        Duration.QUARTER_DOT + Duration.QUARTER,
    ])
    assert comp._pattern_complexity(p4) == 9
